\begin{question}[section=1,subtitle={Ersatzschaltbild im Leerlauf},difficulty=,mode=exm,type=thr,tags={}]
Wie schaut das Ersatzschaltbild vom Trafo im Leerlauffall und Kurzschlussfall aus?
\end{question}
\begin{solution}
\begin{figure}
\centering
\includegraphics[width=10cm]{\currfiledir image1.png}
\caption{zu A \GetQuestionProperty{counter}{\CurrentQuestionID} Ersatzschaltbild und Zeigerdiagramm im Leerlauf. TODO: Als TikZ Zeichnen.}
\end{figure}
Grundsätzlich ist der Leerlaufstrom eines technischen Trafos sehr klein gegenüber Nennstrom (so lange die Flussdichte im Eisen \bzw Ferrit \etc im mäßig gesättigten Bereich liegt), weil im Gegensatz zu rotierenden Maschinen der unvermeidliche Luftspalt sehr klein ist.
Typische Werte sind \SI{1}{\percent} bis \SI{3}{\percent} des Nennstromes je nach Größe des Trafos.

Im Leerlauf (offene Sekundärwicklung) verhält sich der Trafo wie eine Eisendrossel mit Wirkwiderstand.
Letzterer besteht aus den in diesem Fall kleinen primären Kupferverlusten und den bereits erwähnten Eisenverlusten.
Wir sehen aus dem Ersatzschaltbild, dass der aufgenommene Strom im Wesentlichen von der primären Selbstinduktivität bestimmt ist.
Die Aufteilung in Streu- und Hauptinduktivität ist physikalisch nicht vorhanden.
Der aufgenommene Strom ist weitgehend induktiv, \Dh bei einer sinusförmigen angelegten Spannung eilt er um \ca \SI{90}{\degree} nach.
Da die kleinen ohm'schen Spannungsabfälle senkrecht auf die induktiven Spannungsabfälle stehen, ist deren Einfluss auf den Betrag der induktiven Spannung vernachlässigbar.
Das heißt, dass die angelegte Spannung $\underline{U}_{10}$ gemäß
\begin{equation}
U_1(t) = R_1 I_1(t) + \Diff{\Psi_1(t)}{t}
\end{equation}
oder in komplexer Zeitzeigerrechnung
\begin{equation} \label{glg:u1_zeit}
\underline{U}_1 = R_1 \underline{I}_1 + \j \Omega \underline{\Psi}_1
\end{equation}
den Betrag der Flussverkettung mit der Primärwicklung und damit das Flussdichteniveau im flussführenden Material festlegt.
Daraus folgt, dass die Stromaufnahme, die ja proportional der Summe der magnetischen Spannungsabfälle ist, im Leerlauf die Form der Magnetisierungskennlinie aufweisen wird, wenn man den Eingangsspannungsbetrag variiert.
Der beteiligte Luftspalt bewirkt dabei eine ''Scherung'' der Kennlinie in Richtung ''weniger nichtlinear''.

Prägt man eine zeitlich sinusförmige Spannung ein, ist die daraus resultierende Flussverkettung gemäß \glg{glg:u1_zeit} praktisch sinusförmig.
Bestimmt man nun den Zusammenhang zwischen (augenblicklichen) Werten von Fluss und Magnetierungsstrom $I_\mu$, so kann die zeitliche Stromaufnahme konstruiert werden \abb{abb:magetisierungsstrom}.
\begin{figure}
\centering
\includegraphics[width=10cm]{\currfiledir image2.png}
\caption{zu A \GetQuestionProperty{counter}{\CurrentQuestionID} Form des Magnetisierungsstroms \bzw Leerlaufstroms bei sinusförmigem Flussverlauf und mäßiger Sättigung im Flussmaximum (Strom-Grundschwingung und 3. Oberschwingung getrennt dargestellt)}\label{abb:magetisierungsstrom}
\end{figure}
Bei zeitlich sinusförmigem Verlauf der Eingangsspannung und damit der Flussverkettung treten durch die Nichtlinearität Oberschwingungen im Magnetisierungs- \bzw Leerlaufstrom auf.
Dies ist bei Einphasentransformatoren kein Problem.
Bei Dreiphasentransformatoren kann es vorkommen, dass sich gewisse Harmonsiche im Strom nicht ausgebilden können, weil sie ein Nullsystem darstellen wüden.
\zB liegen die 3. Harmonsiche in jedem Phasenstrom bei sinusförmiger Anspeisung in Phase \textrightarrow~ typisches Nullsystem (vgl. Kapitel E 3.4 Abschnitt ''Nullspannungen und Nullströme'').
Ist kein Mittelpunktsleiter angeschlossen, können diese Harmonischen nicht auftreten.
Dies führt zu Spannungsverzerrung.

\begin{figure}
\centering
\includegraphics[width=10cm]{\currfiledir image3.png}
\caption{zu A \GetQuestionProperty{counter}{\CurrentQuestionID} Kurzschluss-Ersatzschaltbild und Zeitzeigerdiagramm}\label{abb:kurzschluss}
\end{figure}
\begin{subequations}
\begin{align}
R_K &= R_1 + R_2'\\
X_K &= \Omega L_{1\sigma} + \Omega L_{2\sigma}'
\end{align}
\end{subequations}
\bzw definiert man die \underline{Kurzschlussimpedanz}
\begin{equation}
\underline{Z}_K = R_K + \j X_K
\end{equation}
so folgt die einfache Beschreibung des Transformators im Kurzschluss (Index K)
\begin{equation}
\underline{U}_{1K} = (R_K + \j X_K) \underline{I}_{1K} = \underline{Z}_K \underline{I}_{1K}
\end{equation}
Das Spannungsdreieck, welches aus der ohm'schen und der induktiven Komponente in \abb{abb:kurzschluss} gebildet wird, heißt Kapp'sches Dreieck.
Es verändert seine Form bei beliebiger Belastung des Trafos (konstante Frequenz vorausgesetzt) nicht.
Es wird in Abhängigkeit des Stromes nur verschieden groß.

Stellt man am Trafo-Eingang eine solche Spannung ein, dass sich der Nennstrom einstellt, so nennt man diese Spannung die Kurzschlussspannung.
Bezieht man diese auf Nennspannung, so nennt man diese bezogenen Wert die bezogene Kurzschlussspannung $u_K$.
Sie wird meist in Prozent angegeben.
Typische Werte sind \SI{4}{\percent} bei kleinen und \ca \num{10}-\SI{12}{\percent} bei größeren Transformatoren.
\textbf{Beachte:} Die bezogene Kurzschlussspannung gibt an, bei wie viel Prozent der Nennspannung im Kurzschlussfall der Nennstrom fließt.
Umgekehrt bedeutet dies, dass bei Nennspannung der $1/u_K$-fache Nennstrom im Kurzschluss fließt.
Transformatoren werden deshalb auch im Hinblick auf Kurzschlussstrom bezüglich ihrer Streuinduktivität ausgelegt.
Die Streuinduktivität ist, wie aus dem Kapp'schen Dreieck ersichtlich, die Hauptverantwortliche für die Größe der Kurzschlussimpedanz.

\end{solution}
